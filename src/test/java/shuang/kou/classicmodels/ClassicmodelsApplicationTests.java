package shuang.kou.classicmodels;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import shuang.kou.classicmodels.entity.ProductLine;
import shuang.kou.classicmodels.repository.ProductLineRepository;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;


@SpringBootTest
@AutoConfigureMockMvc
public class ClassicmodelsApplicationTests {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ProductLineRepository productLineRepository;

    @Test
    public void should_save_productLine() throws Exception {
        String text = "{\"productLine\":\"test3\",\"textDescription\":\"testtextDescription3\"}";
        mockMvc.perform(post("/productLines").content(text).contentType(MediaType.APPLICATION_JSON_UTF8)).andReturn();
        Optional<ProductLine> test3 = productLineRepository.findById("test3");
        assertTrue(test3.isPresent());
        assertEquals("testtextDescription3", test3.get().getTextDescription());
    }



}
