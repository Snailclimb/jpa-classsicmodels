package shuang.kou.classicmodels.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * @author shuang.kou
 */
@Entity
@Table(name = "productlines")
public class ProductLine {
    @Id
    @Column(name = "productLine", nullable = false, length = 50)
    private String productLine;

    @Column(name = "textDescription")
    @Size(max = 4000)
    private String textDescription;

    public String getProductLine() {
        return productLine;
    }

    public String getTextDescription() {
        return textDescription;
    }

    public ProductLine() {
    }

    public ProductLine(String productLine, @Size(max = 4000) String textDescription) {
        this.productLine = productLine;
        this.textDescription = textDescription;
    }
}

