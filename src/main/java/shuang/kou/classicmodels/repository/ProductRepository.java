package shuang.kou.classicmodels.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shuang.kou.classicmodels.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, String> {

    Page<Product> findAllByOrderByProductCodeAsc(Pageable pageable);

    /**
     * 查找同一生产线的产品
     */
    Page<Product> findByProductLineOrderByProductLineAsc(String productLine,Pageable pageable);


}