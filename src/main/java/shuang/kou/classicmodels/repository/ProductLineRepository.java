package shuang.kou.classicmodels.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import shuang.kou.classicmodels.entity.ProductLine;

@Repository
public interface ProductLineRepository extends JpaRepository<ProductLine, String> {

    Page<ProductLine> findAllByOrderByProductLineAsc(Pageable pageable);

   // ProductLine findByProductLineOrderByProductLineAsc();
}
