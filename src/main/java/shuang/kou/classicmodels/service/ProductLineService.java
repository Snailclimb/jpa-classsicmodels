package shuang.kou.classicmodels.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shuang.kou.classicmodels.entity.ProductLine;
import shuang.kou.classicmodels.exception.IDAlreadyExsitsException;
import shuang.kou.classicmodels.repository.ProductLineRepository;

import java.util.Optional;

@Service
public class ProductLineService {

    private final ProductLineRepository productLineRepository;

    public ProductLineService(ProductLineRepository productLineRepository) {
        this.productLineRepository = productLineRepository;
    }

    public Page<ProductLine> getAllProductLines(Pageable pageable) {
        return productLineRepository.findAllByOrderByProductLineAsc(pageable);
    }

    public ProductLine createProductLine(ProductLine productLine) {
        Optional<ProductLine> productLineOptional = productLineRepository.findById(productLine.getProductLine());
        if (productLineOptional.isPresent()) {
            throw new IDAlreadyExsitsException("ProductLine:" + productLineOptional.get().getProductLine() + "Already Exsits!");
        }
        return productLineRepository.save(productLine);

    }
}
