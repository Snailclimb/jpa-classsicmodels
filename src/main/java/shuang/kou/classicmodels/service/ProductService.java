package shuang.kou.classicmodels.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import shuang.kou.classicmodels.entity.Product;
import shuang.kou.classicmodels.exception.ResourseNotFoundException;
import shuang.kou.classicmodels.repository.ProductLineRepository;
import shuang.kou.classicmodels.repository.ProductRepository;


@Service
public class ProductService {


    private ProductRepository productRepository;
    private ProductLineRepository productLineRepository;

    public ProductService(ProductRepository productRepository, ProductLineRepository productLineRepository) {
        this.productRepository = productRepository;
        this.productLineRepository = productLineRepository;
    }


    public Page<Product> getAllProducts(Pageable pageable) {
        return productRepository.findAllByOrderByProductCodeAsc(pageable);
    }

    public Product getProductByID(String id) {
        return productRepository.findById(id).orElseThrow(() -> new ResourseNotFoundException("productCode:" + id + "not found!"));

    }

    public Page<Product> getProductByProductLine(String productLine, Pageable pageable) {
        return productRepository.findByProductLineOrderByProductLineAsc(productLine, pageable);

    }

    public Product createProduct(String productLineId, Product product) {
        return productLineRepository.findById(productLineId).map(productLine -> {
            product.setProductLine(productLine);
            return productRepository.save(product);
        }).orElseThrow(() -> new ResourseNotFoundException("productLineId:" + productLineId + "not found!"));

    }

}
