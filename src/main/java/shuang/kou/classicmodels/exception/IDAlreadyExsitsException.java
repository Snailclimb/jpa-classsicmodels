package shuang.kou.classicmodels.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class IDAlreadyExsitsException extends RuntimeException {

    public IDAlreadyExsitsException() {
        super();
    }

    public IDAlreadyExsitsException(String message) {
        super(message);
    }

    public IDAlreadyExsitsException(String message, Throwable cause) {
        super(message, cause);
    }


}
