package shuang.kou.classicmodels.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import shuang.kou.classicmodels.entity.ProductLine;
import shuang.kou.classicmodels.service.ProductLineService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProductLineController {
    private final ProductLineService productLineService;

    public ProductLineController(ProductLineService productLineService) {
        this.productLineService = productLineService;
    }

    @GetMapping("/product-lines")
    public Page<ProductLine> getALLProductLines(Pageable pageable) {
        return productLineService.getAllProductLines(pageable);
    }


    @PostMapping("/product-lines")
    public ProductLine createProductLine(@RequestBody @Valid ProductLine productLine) {
        System.out.println(productLine.toString());
        return productLineService.createProductLine(productLine);
    }
}
