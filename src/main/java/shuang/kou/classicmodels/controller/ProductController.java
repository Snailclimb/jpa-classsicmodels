package shuang.kou.classicmodels.controller;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import shuang.kou.classicmodels.entity.Product;
import shuang.kou.classicmodels.service.ProductService;

import javax.validation.Valid;

@RestController
@RequestMapping("/api")
public class ProductController {
    private final ProductService productService;

    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    @GetMapping("/products")
    public Page<Product> getAllProducts(Pageable pageable) {
        return productService.getAllProducts(pageable);
    }

    @GetMapping("/products/{id}")
    public Product getAllProductsById(@PathVariable String id) {
        return productService.getProductByID(id);
    }

    /**
     * TODO：通过productLine查询Product，productLine这里不应该是String类型
     */
    @GetMapping("/products/product-lines")
    public Page<Product> getAllProductsByProductLine(@RequestParam String productLine, Pageable pageable) {
        return productService.getProductByProductLine(productLine, pageable);
    }

    @PostMapping("/{id}/products")
    public Product createProduct(@PathVariable String id, @Valid @RequestBody Product product) {

        return productService.createProduct(id, product);
    }

}
